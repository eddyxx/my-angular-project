import {cert} from "./cred/cert";
import * as admin from "firebase-admin";
import * as express from "express";
import * as cors from "cors";
import * as bodyParser from "body-parser"
import {getAllMovies, getMovieById, getMovieByKind} from "./services/movies.service";
import {MovieModel} from "./models/movie.model";
import {deleteUserById, getUserById, postNewUser, updateUser} from "./services/user.service";
import {UserModel} from "./models/users.model";


admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://cinenightmare-9191a.firebaseio.com"

})
const app = express();
app.use(cors());
app.use(bodyParser.json());



app.get('/api/movies', async (req, res) => {
    try {
        const movies = await getAllMovies();
        return res.send(movies);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.get('/api/movies/:movieId', async (req, res) => {
    try {
        const movieId: string = req.params.movieId;
        const movieToFind: MovieModel = await getMovieById(movieId);
        return res.send(movieToFind);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.get('/api/movies/:MovieId/movie kind', async (req, res) => {
    try {
        const MovieKind: string = req.params.movieKind;
        const MovieKindToFind: MovieModel[] = await getMovieByKind(MovieKind);
        return res.send(MovieKindToFind);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.post('/api/users', async (req, res) => {
    try {
        const newUser = req.body;
        const addResult = await postNewUser(newUser);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.patch('/api/users/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const user = await updateUser(id, req.body);
        return res.send(user);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.delete('/api/users/:userId/', async (req, res) => {
    try {

        const userId: string = req.params.userId;
        const result: string = await deleteUserById(userId);
        return res.send(result);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.get('/api/users/:userId', async (req, res) => {
    try {
        const userId: string = req.params.userId;
        const userToFind: UserModel[] = await getUserById(userId);
        return res.send(userToFind);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
