import {MovieModel} from "./models/movie.model";


export const moviesData: MovieModel[] = [
    {
        id: 'EwZItMDNQ5c2BTn12cRK',
        MovieName: 'alien',
        AnneeDeSortie: 1979,
        videoUrl:"https://www.youtube.com/watch?v=cL5aAtL6To",
        Duree: '1h57',
        MovieKind: 'sf/horreur',
        realisateur: 'ridley scott',
        synopsis:'Durant le voyage de retour d\'un immense cargo spatial en mission commerciale de routine, ses passagers, cinq hommes et deux femmes plongés en hibernation, sont tirés de leur léthargie dix mois plus tôt que prévu par Mother, l\'ordinateur de bord. Ce dernier a en effet capté dans le silence interplanétaire des signaux sonores, et suivant une certaine clause du contrat de navigation, les astronautes sont chargés de prospecter tout indice de vie dans l\'espace .'
    },
    {
        id: '0NiY9hei24ZuPcQNOedP',
        MovieName: 'maniac',
        AnneeDeSortie: 1980,
        videoUrl:"https://www.youtube.com/watch?v=S-UHzcNYlGo",
        Duree: '1h28',
        MovieKind: 'slasher',
        realisateur: 'William Lustig',
        synopsis:'Victime des mauvais traitements infligés par sa mère lorsqu\'il était enfant, Frank Zito en est resté profondément traumatisé, hanté par d\'horribles rêves et impuissant. Un soir, dans l\'espoir d\'apaiser ses angoisses, il quitte son appartement sordide et se lance à la recherche d\'une prostituée dans les rues de New York. L\'expérience est un cuisant échec. Fou de rage, il étrangle la malheureuse et emporte son scalp comme trophée.'
    },
    {
        id:'5kZGOH3J7ruh8F20HccF',
        MovieName: 'predator',
        AnneeDeSortie: 1987,
        videoUrl:"https://www.youtube.com/watch?v=9Rma_uO81dw",
        Duree: '1h47',
        MovieKind: 'sf/horreur',
        realisateur: 'John McTiernan',
        synopsis:'Le major Dutch Schaeffer prend la tête d\'un commando chargé de délivrer un groupe de civils américains prisonniers de guérilleros en Amérique centrale. Largués en hélicoptère dans une jungle hostile, ses hommes et lui découvrent des corps de soldats écorchés vifs... avant d\'être pris en chasse par une mystérieuse créature.'
    },
    {
        id:'7VHZ1tmTrz0sE9I11Q5R',
        MovieName: 'halloween',
        AnneeDeSortie: 1978,
        videoUrl:"https://www.youtube.com/watch?v=w_pSY0KTh0A",
        Duree: '1h30',
        MovieKind: 'slasher',
        realisateur: 'John Carpenter',
        synopsis:'La nuit d\'Halloween 1963. Le jeune Michael Myers se précipite dans la chambre de sa soeur aînée et la poignarde sauvagement. Après son geste, Michael se mure dans le silence et est interné dans un asile psychiatrique. Quinze ans plus tard, il s\'échappe de l\'hôpital et retourne sur les lieux de son crime. Il s\'en prend alors aux adolescents de la ville.'
    },
    {
        id: 'AdX3bR5Q6TwXSpV5KhjA',
        MovieName: 'jusquen enfer',
        AnneeDeSortie: 2009,
        videoUrl:"https://www.youtube.com/watch?v=CdxLR_OUIq0",
        Duree: '1h39',
        MovieKind: 'comédie/horreur',
        realisateur: 'sam raimi',
        synopsis:'Christine Brown travaille dans une société de crédit. Elle espère vraiment avoir le poste d\'assistant qui vient de se libérer. Alors, quand madame Ganush, une vieille gitane, vient lui demander un prêt pour pouvoir conserver son logement, elle le lui refuse pour montrer à son supérieur, monsieur Jacks, qu\'elle est capable de ce genre de décision."\n'

    },
    {
        id:'Byt9satyNoMJCtCV0PWM',
        MovieName: 'creep',
        AnneeDeSortie: 2015,
        videoUrl:"https://www.youtube.com/watch?v=O3Fo4SXvLVg",
        Duree: '1h22',
        MovieKind: 'found footage',
        realisateur: 'Patrick Kack-Brice',
        synopsis:'Aaron, un réalisateur sans le sou, accepte de se rendre dans un chalet éloigné pour un clien, Josef. Josef explique à Aaron qu\'il ne lui reste que peu de temps à vivre car il est atteint d\'une tumeur au cerveau inopérable. Il va mourir avant que sa femme enceinte, Angela n\'accouche et demande à Aaron de l\'aider à enregistrer un journal vidéo pour son enfant à naître. Mais Josef est excentrique, ce qui met Aaron mal à l\'aise jusqu\'au point où Josef révèle qu\'il a violé sa femme. Se sentant en danger, Aaron cherche ses clefs de voiture mais ne les trouve pas. Il intercepte un appel téléphonique d\'Angela, qui est en fait la sœur de Josef qui l\'enjoint à fuir le plus vite possible.'
    },
    {

        id:'Fs4A8pT6xDQpSZ6WhnK2',
        MovieName: 'rec',
        AnneeDeSortie: 2005,
        videoUrl:"https://www.youtube.com/watch?v=rRVqQMQQ1uE",
        Duree: '1h18',
        MovieKind: 'found footage',
        realisateur: 'Jaume Balagueró, Paco Plaza',
        synopsis:'Une jeune journaliste et un collègue suivent des pompiers pour une intervention dans un appartement. Prise à son propre piège, la journaliste est enfermée avec le scoop qu\'elle désirait ardemment. Elle embarque le spectateur dans un huis clos terrifiant quand un nouveau virus transforme les infectés en dangereux zombies.'


    },
    {

        id:'GqCqoR4t7p5J2Cp5vr9M',
        MovieName: 'massacre a la tronçonneuse',
        AnneeDeSortie: 1982,
        videoUrl:"https://www.youtube.com/watch?v=AmEpPJ1MKP8&t=4s",
        Duree: '1h30',
        MovieKind: 'slasher',
        realisateur: 'tobe hooper',
        synopsis:'L\'histoire se concentre autour de la famille Hewitt qui habite dans un endroit reculé du Texas. L\'abattoir de la ville ferme et la région se vide ainsi de ses emplois. La famille décide de rester malgré tout et le jeune Thomas, licencié, se venge de son patron en le tuant. Le shérif veut l\'arrêter, mais son oncle abat le représentant de la loi et endosse désormais le costume de l\'autorité. Les Hewitt deviennent les maîtres de leur ville et pour subsister, ils n\'ont besoin que d\'un peu de nourriture que les badauds anonymes leur apporteront..'

    },
    {
        id:'L1ofojS70rUBaQLPMawm',
        MovieName: 'street trash',
        AnneeDeSortie: 1987,
        videoUrl:"https://www.youtube.com/watch?v=2GpzUWdHzks",
        Duree: '1h42',
        MovieKind: 'nanar',
        realisateur: 'James Michael Muro',
        synopsis:'Dans les tréfonds de sa réserve, le propriétaire d’une petite boutique de spiritueux découvre une caisse d’un alcool frelaté de marque Viper. Les bouteilles trouvent très vite preneurs, achetés par les marginaux et clochards qui peuplent une casse automobile du quartier. Ses effets sont dévastateurs, le breuvage d’origine inconnue décapant de l’intérieur ceux qui l’ingurgitent.'

    },
    {
        id: 'NPQjYkku2O5gH9iwo3vj',
        MovieName: 'le projet blair witch',
        AnneeDeSortie: 1999,
        videoUrl:"https://www.youtube.com/watch?v=m1IKhSZ6OYM",
        Duree: '1h57',
        MovieKind: 'found footage',
        realisateur: 'Eduardo Sánchez, Daniel Myrick',
        synopsis:'Le 21 octobre 1994, 3 étudiants de cinéma vont dans la forêt de Black Hills, dans le Maryland, pour faire un documentaire sur une légende locale : la sorcière de Blair. Un an plus tard, leur bande vidéo est retrouvée. Eux sont toujours portés disparus."'
    },
    {

        id: 'SSjFQ6F6naV6HTGUiFYM',
        MovieName: 'the toxic avenger',
        AnneeDeSortie: 1987,
        videoUrl:"https://www.youtube.com/watch?v=SH_2-8VuTFk",
        Duree: '1h50',
        MovieKind: 'nanar',
        realisateur: 'Lloyd Kaufman',
        synopsis:'Melvin Junko, technicien de surface dans une piscine, est le souffre-douleur de la bande à Bozo, psychopathe dont le passe-temps consiste à écraser des enfants avec sa voiture. Tombé dans une embuscade destinée à l\'humilier publiquement, Melvin prend la fuite, se défenestre et tombe dans un fût radioactif transporté par un camion arrêté à cet endroit, ce qui le transformera en un monstre hideux et surpuissant. Il deviendra justicier sous le surnom de Toxic Avenger. Mais le maire de la ville, parrain d\'une mafia locale, voit d\'un très mauvais oeil l\'arrivée de ce super-héros.'

    },
    {
        id:'SsyFLG59LTZo5gbch8NO',
        MovieName: 'le projet atticus',
        AnneeDeSortie: 2015,
        videoUrl:"https://www.youtube.com/watch?v=seH1SmcFPig",
        Duree: '1h32',
        MovieKind: 'found footage',
        realisateur: 'Chris Sparling',
        synopsis:'Fondé en 1976 par le Dr Henry West, l\'Institut Atticus était spécialisé dans l\'étude de personnes développant des capacités paranormales : parapsychologie, voyance, psychokinésie, etc. Des centaines de personnes présentant ce genre d\'aptitudes ont été étudiées par les chercheurs de l\'institut et de nombreux articles annonçant leurs résultats ont été publiés. Aucun cas étudié jusque-là n\'avait préparé le Dr West et son équipe à l\'arrivée de Judith Winstead."'
    },
    {
        id: 'Sy8MUg89mREkgOBJNMFK',
        MovieName: 'shaun of the dead',
        AnneeDeSortie: 2004,
        videoUrl:"https://www.youtube.com/watch?v=s0RaHzRP-L4",
        Duree: '1h40',
        MovieKind: 'comédie/horreur',
        realisateur: 'Edgar wright',
        synopsis:'Shaun ne sait pas quoi faire de sa vie, et encore moins de sa relation de couple. Heureusement, une hallucinante invasion de zombies va lui remettre les idées en place.'
    },
    {
        id: 'WPtUtrA3EbFAn7DVyVjO',
        MovieName: 'ghost to mars',
        AnneeDeSortie: 2001,
        videoUrl:"https://www.youtube.com/watch?v=TAodkwmaJdU",
        Duree: '1h38',
        MovieKind: 'sf/horreur',
        realisateur: 'john carpenter',
        synopsis:'En 2176, Mars, la planète rouge, est colonisée par les êtres humains pour éviter la surpopulation sur Terre. Son sol riche en ressources naturelles permet à plusieurs milliers de personnes de survivre et de travailler. Une équipe de mineurs tombent d\'ailleurs sur une sépulture et libèrent par le même coup les âmes d\'anciens guerriers d\'une race martienne. Ces derniers décident de se venger en s\'introduisant dans les corps humains pour les éliminer'
    },
    {
        id:'bIJ8H5ZR2m035gRWjaDS',
        MovieName: 'tucker et dale fightent le mal',
        AnneeDeSortie: 2008,
        videoUrl:"https://www.youtube.com/watch?v=yocEi2ZLQdE",
        Duree: '1h29',
        MovieKind: 'comédie horrifique',
        realisateur: 'Eli craig',
        synopsis:'Tucker et Dale sont deux gentils péquenauds venus se ressourcer en forêt. Ils y rencontrent des étudiants venus faire la fête. Suite à un quiproquo entraînant la mort d\'un des jeunes, ces derniers pensent que Tucker et Dale sont des serial killers qui veulent leur peau, alors que nos héros pensent que les jeunes font partie d\'une secte et qu\'ils sont là pour un suicide collectif ! C\'est le début d\'un gigantesque malentendu dans lequel horreur et hilarité vont se mélanger.'
    },
    {
        id: 'dSB05J0QieFvzm4mnZjk',
        MovieName: 'vendredi 13',
        AnneeDeSortie: 1981,
        videoUrl:"https://www.youtube.com/watch?v=qvn74FqQiQk",
        Duree: '1h36',
        MovieKind: 'slasher',
        realisateur: 'Sean S. Cunningham',
        synopsis:'Fermé depuis une vingtaine d\'années suite à de mystérieux accidents, le camp de vacances "Crystal Lake" va être rouvert au public. Engagée comme cuisinière, l\'étudiante Annie fait de l\'auto-stop pour rejoindre ses compagnons. Montée dans un Jeep, elle est assassinée par son conducteur."'
    },
    {
        id: 'eBgWqPiYIx8pCVGwpFTx',
        MovieName: 'evil dead 2',
        AnneeDeSortie: 1987,
        videoUrl:"https://www.youtube.com/watch?v=5Fu4WZ6KWgU",
        Duree: '1h24',
        MovieKind: 'comédie horrifique',
        realisateur: 'sami raimi',
        synopsis:'Ash et Linda retournent dans la cabane où, quatre ans plus tôt, ils avaient été attaqués par les démons qui hantaient les bois environnants'
    },
    {
        id:'f2auK3LwdkenA2bxCprc',
        MovieName: 'prometheus',
        AnneeDeSortie: 2012,
        videoUrl:"https://www.youtube.com/watch?v=5yJ-gkTuao4",
        Duree: '2h',
        MovieKind: 'sf/horreur',
        realisateur: 'ridley scott',
        synopsis:'En 2093, des scientifiques cherchent à dépasser leurs limites mentales et physiques et tentent d\'explorer ce qu\'il y a au-delà du possible. Ils vont être amenés à découvrir un indice sur l\'origine de l\'humanité sur Terre. Cette découverte les entraîne dans un voyage fascinant jusqu\'aux recoins les plus sombres de l\'univers. Là-bas, un affrontement terrifiant qui décidera de l\'avenir de l\'humanité les attend.'
    },
    {
        id: 'io7aBK8zR7WfU4esTHmU',
        MovieName: 'paranormal acctivity',
        AnneeDeSortie: 2009,
        videoUrl:"https://www.youtube.com/watch?v=EuXfQvc85mk",
        Duree: '1h10',
        MovieKind: 'found footage',
        realisateur: 'oren peli',
        synopsis:'Un jeune couple s\'installe dans une maison en banlieue. Le bonheur fait place à la terreur lorsqu\'ils doivent repousser, toutes les nuits, les attaques d\'un esprit démoniaque...'
    },
    {
        id: 'raXe8rSjxiBgX3z6RDcq',
        MovieName: 'scream',
        AnneeDeSortie: 1997,
        videoUrl:"https://www.youtube.com/watch?v=d7yWagIcCLE",
        Duree: '1h51',
        MovieKind: 'slasher',
        realisateur: 'wes craven',
        synopsis:'Casey Becker, une belle adolescente, est seule dans la maison familiale. Elle s\'apprête à regarder un film d\'horreur, mais le téléphone sonne. Au bout du fil, un serial killer la malmène, et la force à jouer à un jeu terrible : si elle répond mal à ses questions portant sur les films d\'horreur, celui-ci tuera son copain. Sidney Prescott sait qu\'elle est l\'une des victimes potentielles du tueur de Woodsboro. Celle-ci ne sait plus à qui faire confiance.'
    },
    {
        id:'sVxoLFKkzvqHPS7xtf3I',
        MovieName: 'brain dead',
        AnneeDeSortie: 1992,
        videoUrl:"https://www.youtube.com/watch?v=MFT1iP1RS4I",
        Duree: '1h44',
        MovieKind: 'nanar',
        realisateur: 'peter jackson',
        synopsis:'En 1957, Lionel, un jeune homme célibataire et timoré, voit sa mère, quelque peu tyrannique, se faire mordre par un singe-rat de Sumatra dans un zoo. D\'après la légende des indigènes de cette île, ce singe-rat est maudit et transforme quiconque se fait mordre en zombie. Et en effet, quelque temps après Lionel perçoit quelques troubles dans le comportement de sa mère qui se transforme peu à peu en zombie. Lionel essaie d\'abord de la guérir mais sans succès. Peu à peu, l\'infection se répand mais plutôt que de tenter de se débarrasser des monstres, Lionel les enferme dans la cave de sa maison. Deux zombies, un prêtre et une infirmière, ont un rapport sexuel, ce qui donne naissance à un enfant zombie. En tentant de les mettre « à mort » définitivement, il se trompe de substance et leur donne des stimulants ce qui a pour effet de les rendre complètement enragés. Avec l\'aide de sa petite amie Paquita, Lionel réussit enfin à éliminer tous les zombies dont le plus sanguinaire, sa mère transformée en un gigantesque monstre, et ce de manière gorissime avec notamment une tondeuse à gazon.'
    },
    {

        id: 'sVxoLFKkzvqHPS7xtf3I',
        MovieName: 'bad taste',
        AnneeDeSortie: 1987,
        videoUrl:"https://www.youtube.com/watch?v=wIwnIQRmT60",
        Duree: '1h32',
        MovieKind: 'nanar',
        realisateur: 'peter jackson',
        synopsis:'Une bande d\'extraterrestres anthropophages déguisés en humains envahissent un petit village néo-zélandais dans le but de « moissonner » des humains pour leur chaîne de fast-foods. Ils se voient repoussés par une poignée de résistants hargneux et armés jusqu\'aux dents.'
    },
    {
        id: 'wGAPHZdd500HZG1Wi9sw',
        MovieName: 'the void',
        AnneeDeSortie: 2016,
        videoUrl:"https://www.youtube.com/watch?v=v7W9Hb0Wv-U",
        Duree: '1h30',
        MovieKind: 'sf/horreur',
        realisateur: 'Steven Kostanski, Jeremy Gillespie',
        synopsis:'Un agent de police découvre un homme ensanglanté sur une route déserte et l\'emmène en urgence à l\'hôpital. Rapidement, d\'étranges individus entourent le lieu et les patients commencent à devenir fous. Le policier va alors découvrir, à l\'intérieur de l\'hôpital, une porte vers un monde démoniaque.'
    },
    {

        id:'zGr1YAoSSAjMAuhnWhTj',
        MovieName: 'bienvenue a zombieland',
        AnneeDeSortie: 2009,
        videoUrl:"https://www.youtube.com/watch?v=NPbsp_nFj7M",
        Duree: '1h28',
        MovieKind: 'comédie horrifique',
        realisateur: 'ruben fleischer',
        synopsis:'Suite à une mutation du virus de la vache folle, les humains sont transformés en zombies. Deux survivants que tout oppose, sillonnent les routes et affrontent les zombies qui grouillent aux quatre coins du pays'
    },
];



