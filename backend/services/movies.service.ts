import * as admin from "firebase-admin";
import QuerySnapshot = admin.firestore.QuerySnapshot;
import {MovieModel} from "../models/movie.model";
import DocumentSnapshot = admin.firestore.DocumentSnapshot;

const db = admin.firestore();

const refMovies = db.collection('movies');

export async function getAllMovies(): Promise<MovieModel[]> {
    const moviesQuerySnap: QuerySnapshot = await refMovies.get();
    const movies: MovieModel[] = [];
    moviesQuerySnap.forEach(movieSnap => movies.push(movieSnap.data() as MovieModel));
    return movies;
}

export async function getMovieById(movieId: string): Promise<MovieModel> {
    if (!movieId) {
        throw new Error(`movieId required`);
    }
    const movieToFindRef = refMovies.doc(movieId);
    const movieToFindSnap: DocumentSnapshot = await movieToFindRef.get();
    if (movieToFindSnap.exists) {
        return movieToFindSnap.data() as MovieModel;
    } else {
        throw new Error('object does not exists');
    }
     }

    export async function getMovieByKind(MovieKind: string): Promise<MovieModel[]> {
        if (!MovieKind) {
            throw new Error(`Movie Kind required`);
        }

        const data = await refMovies
            .where('movie kind', '==', MovieKind)
            .get();
        const result: MovieModel[] = [];

        data.forEach(doc => result.push(doc.data() as MovieModel));
        return result;

    }

