import * as admin from "firebase-admin";
import {UserModel} from "../models/users.model";
import DocumentReference = admin.firestore.DocumentReference;
import DocumentData = admin.firestore.DocumentData;
import DocumentSnapshot = admin.firestore.DocumentSnapshot;


const db = admin.firestore();

const refUsers = db.collection('users');



export async function postNewUser(newUser: UserModel): Promise<UserModel> {
    if (!newUser) {
        throw new Error(`new user must be filled`);
    }
    const addResult: DocumentReference<DocumentData> = await refUsers.add(newUser);
    return {...newUser, id: addResult.id};
}
export async function updateUser(userId: string, newUser: UserModel) : Promise<UserModel>{
    if (!newUser || !userId) {
        throw new Error(` data user and user id must be filled`);
    }

    const userToPutRef: DocumentReference = await testIfHostelExistsById(userId);
    await userToPutRef.update(newUser);
    return newUser;

}

async function testIfHostelExistsById(userId: string): Promise<DocumentReference> {
    const userRef: DocumentReference = refUsers.doc(userId);
    const snapUserToUpdate: DocumentSnapshot = await userRef.get();
    const userToUpdate: UserModel | undefined = snapUserToUpdate.data() as UserModel | undefined;
    if (!userToUpdate) {
        throw new Error(`${userId} does not exists`);
    }
    return userRef;
}

export async function getUserById(userId: string): Promise<UserModel[]> {
    if (!userId) {
        throw new Error(`userId required`);
    }
    const userToFindRef = refUsers.doc(userId);
    const userToFindSnap: DocumentSnapshot = await userToFindRef.get();
    if (userToFindSnap.exists) {
        return userToFindSnap.data() as UserModel[];
    } else {
        throw new Error('object does not exists');
    }
}

export async function deleteUserById(userId: string): Promise<any> {
    if (!userId) {
        throw new Error('user id is missing');
    }
    const userToDeleteRef: DocumentReference = await testIfHostelExistsById(userId);
    await userToDeleteRef.delete();
    return {response : `${userId} -> delete ok` };
}
