import {MovieModel} from "./movie.model";

export interface UserModel {
    id?:  string;
    name: string;
    Email: string[];
    listData?: MovieModel[];

}
