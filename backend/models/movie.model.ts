
import {VideosModel} from "./videos.model";

export interface MovieModel {
  id?:string;
  MovieName?: string;
  AnneeDeSortie?: number;
  videoUrl?: string|number;
  Duree?: string|number;
  MovieKind?: string;
  realisateur?: string;
  synopsis?:string;
  videoData?: VideosModel[];


}
