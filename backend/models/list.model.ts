
import {MovieModel} from './movie.model';

export interface ListModel {
    id?:  string;
    nameList: string;
    Movies: string[];
    MovieData?: MovieModel[];

}

