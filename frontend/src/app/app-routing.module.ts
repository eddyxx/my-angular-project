import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/movies' },
  { path: 'lists', loadChildren: () => import('./pages/lists/lists.module').then(m => m.ListsModule) },
  { path: 'movies', loadChildren: () => import('./pages/movies/movies.module').then(m => m.MoviesModule) },
  { path: 'epouvante', loadChildren: () => import('./pages/genres/epouvante/epouvante.module').then(m => m.EpouvanteModule) },
  { path: 'slasher', loadChildren: () => import('./pages/genres/slasher/slasher.module').then(m => m.SlasherModule) },
  { path: 'nanar', loadChildren: () => import('./pages/genres/nanar/nanar.module').then(m => m.NanarModule) },
  { path: 'comediehorrifique', loadChildren: () => import('./pages/genres/comediehorrifique/comediehorrifique.module').then(m => m.ComediehorrifiqueModule) },
  { path: 'sfhorreur', loadChildren: () => import('./pages/genres/sfhorreur/sfhorreur.module').then(m => m.SfhorreurModule) },
  { path: 'foundfootage', loadChildren: () => import('./pages/genres/foundfootage/foundfootage.module').then(m => m.FoundfootageModule) },
  { path: 'user', loadChildren: () => import('./pages/user/user.module').then(m => m.UserModule) },
  { path: 'videos', loadChildren: () => import('./pages/videos/videos.module').then(m => m.VideosModule) },
  { path: 'alien', loadChildren: () => import('./pages/all movie pages/alien/alien.module').then(m => m.AlienModule) },
  { path: 'maniac', loadChildren: () => import('./pages/all movie pages/maniac/maniac.module').then(m => m.ManiacModule) },
  { path: 'predator', loadChildren: () => import('./pages/all movie pages/predator/predator.module').then(m => m.PredatorModule) },
  { path: 'halloween', loadChildren: () => import('./pages/all movie pages/halloween/halloween.module').then(m => m.HalloweenModule) },
  { path: 'jusquEnenfer', loadChildren: () => import('./pages/all movie pages/jusqu-enenfer/jusqu-enenfer.module').then(m => m.JusquEnenferModule) },

];

@NgModule({

  imports: [RouterModule.forRoot(routes)]

})
export class AppRoutingModule { }
