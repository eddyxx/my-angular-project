// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: "AIzaSyC8Is72FqFxS9_GMLUlb9Y8MDLqaKVcDz0",
  authDomain: "cinenightmare-9191a.firebaseapp.com",
  databaseURL: "https://cinenightmare-9191a.firebaseio.com",
  projectId: "cinenightmare-9191a",
  storageBucket: "cinenightmare-9191a.appspot.com",
  messagingSenderId: "399637908137",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
