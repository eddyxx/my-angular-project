import {VideosModel} from "./models/videos.model";

export class movie {

  constructor(
   public  MovieName: string,
   public  AnneeDeSortie: number,
   public BandeAnnonce: boolean,
   public Duree: number| string,
   public MovieKind: string,
   public realisateur: string,
   public synopsis:string,
   public videoData?: VideosModel[]

  ) {  }

}
