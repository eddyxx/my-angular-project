import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableMoviesComponent } from './table-movies/table-movies.component';
import {NzDividerModule, NzTableModule} from "ng-zorro-antd";



@NgModule({
  declarations: [TableMoviesComponent],
  imports: [
    CommonModule,
    NzDividerModule,
    NzTableModule
  ]
})
export class TablesModule { }
