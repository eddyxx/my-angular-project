import {MovieModel} from "./movie.model";

export interface UserModel {
    id?: number | string;
    name: string;
    Email: string[];
    listData?: MovieModel[];

}
