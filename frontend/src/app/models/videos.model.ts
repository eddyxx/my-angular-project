import {MovieModel} from "./movie.model";

export interface VideosModel {
    id?: string;
    movieName: string;
    UrlVideo: number| string;
    MovieData?: MovieModel[];
 }
