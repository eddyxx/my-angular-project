import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alien',
  templateUrl: './alien.component.html',
  styleUrls: ['./alien.component.less']
})

export class AlienComponent implements OnInit {


  tooltips = ['terrible', 'bad', 'normal', 'good', 'wonderful'];
  value = 4;



  constructor() { }

  ngOnInit(): void {
  }

  onBack() {
    console.log('onBack')

  }
}
