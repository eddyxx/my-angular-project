import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlienRoutingModule } from './alien-routing.module';
import { AlienComponent } from './alien.component';
import {
  NzAnchorModule,
  NzDescriptionsModule,
  NzIconModule,
  NzListModule,
  NzPageHeaderModule, NzRateModule,
  NzSkeletonModule,
  NzSwitchModule
} from "ng-zorro-antd";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [AlienComponent],
  imports: [
    CommonModule,
    AlienRoutingModule,
    NzPageHeaderModule,
    NzSwitchModule,
    NzListModule,
    FormsModule,
    NzSkeletonModule,

    NzIconModule,
    NzDescriptionsModule,
    NzAnchorModule,
    NzRateModule
  ]
})
export class AlienModule { }
