import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlienComponent } from './alien.component';

const routes: Routes = [{ path: '', component: AlienComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlienRoutingModule { }
