import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreepComponent } from './creep.component';

const routes: Routes = [{ path: '', component: CreepComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreepRoutingModule { }
