import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreepRoutingModule } from './creep-routing.module';
import { CreepComponent } from './creep.component';
import {NzDescriptionsModule, NzPageHeaderModule, NzRateModule} from "ng-zorro-antd";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [CreepComponent],
  imports: [
    CommonModule,
    CreepRoutingModule,
    NzPageHeaderModule,
    FormsModule,
    NzRateModule,
    NzDescriptionsModule
  ]
})
export class CreepModule { }
