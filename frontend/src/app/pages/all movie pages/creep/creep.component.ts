import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-creep',
  templateUrl: './creep.component.html',
  styleUrls: ['./creep.component.less']
})
export class CreepComponent implements OnInit {
  tooltips = ['terrible', 'bad', 'normal', 'good', 'wonderful'];
  value = 4;


  constructor() { }

  ngOnInit(): void {
  }

  onBack() {
    console.log('onBack')
  }
}
