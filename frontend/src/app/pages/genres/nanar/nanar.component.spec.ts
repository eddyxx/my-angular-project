import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NanarComponent } from './nanar.component';

describe('NanarComponent', () => {
  let component: NanarComponent;
  let fixture: ComponentFixture<NanarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NanarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NanarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
