import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NanarComponent } from './nanar.component';

const routes: Routes = [{ path: '', component: NanarComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NanarRoutingModule { }
