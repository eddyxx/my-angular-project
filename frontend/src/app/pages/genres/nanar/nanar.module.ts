import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NanarRoutingModule } from './nanar-routing.module';
import { NanarComponent } from './nanar.component';
import {NzDividerModule, NzPageHeaderModule, NzTableModule} from "ng-zorro-antd";


@NgModule({
  declarations: [NanarComponent],
  imports: [
    CommonModule,
    NanarRoutingModule,
    NzPageHeaderModule,
    NzDividerModule,
    NzTableModule
  ]
})
export class NanarModule { }
