import {Component, Input, OnInit} from '@angular/core';
import {MovieModel} from "../../../models/movie.model";
import {Subject} from "rxjs";
import {NanarService} from "../../../services/nanar.service";
import {switchMap, takeUntil, tap} from "rxjs/operators";

interface movies {

  MovieName: string;
  AnneeDeSortie: number;
  Duree: number| string;
  MovieKind: string;
  realisateur: string;

}

@Component({
  selector: 'app-nanar',
  templateUrl: './nanar.component.html',
  styleUrls: ['./nanar.component.less']
})
export class NanarComponent implements OnInit {
  listOfData: movies[] = [
    {

      MovieName: 'street trash',
      AnneeDeSortie: 1987,
      Duree: '1h42',
      MovieKind: 'nanar',
      realisateur: 'James Michael Muro',
      //synopsis:'Dans les tréfonds de sa réserve, le propriétaire d’une petite boutique de spiritueux découvre une caisse d’un alcool frelaté de marque Viper. Les bouteilles trouvent très vite preneurs, achetés par les marginaux et clochards qui peuplent une casse automobile du quartier. Ses effets sont dévastateurs, le breuvage d’origine inconnue décapant de l’intérieur ceux qui l’ingurgitent.'

    },
    {

      MovieName: 'the toxic avenger',
      AnneeDeSortie: 1987,
      Duree: '1h50',
      MovieKind: 'nanar',
      realisateur: 'Lloyd Kaufman',
      // synopsis:'Melvin Junko, technicien de surface dans une piscine, est le souffre-douleur de la bande à Bozo, psychopathe dont le passe-temps consiste à écraser des enfants avec sa voiture. Tombé dans une embuscade destinée à l\'humilier publiquement, Melvin prend la fuite, se défenestre et tombe dans un fût radioactif transporté par un camion arrêté à cet endroit, ce qui le transformera en un monstre hideux et surpuissant. Il deviendra justicier sous le surnom de Toxic Avenger. Mais le maire de la ville, parrain d\'une mafia locale, voit d\'un très mauvais oeil l\'arrivée de ce super-héros.'

    },
    {
      MovieName: 'brain dead',
      AnneeDeSortie: 1992,
      Duree: '1h44',
      MovieKind: 'nanar',
      realisateur: 'peter jackson',
      //  synopsis:'En 1957, Lionel, un jeune homme célibataire et timoré, voit sa mère, quelque peu tyrannique, se faire mordre par un singe-rat de Sumatra dans un zoo. D\'après la légende des indigènes de cette île, ce singe-rat est maudit et transforme quiconque se fait mordre en zombie. Et en effet, quelque temps après Lionel perçoit quelques troubles dans le comportement de sa mère qui se transforme peu à peu en zombie. Lionel essaie d\'abord de la guérir mais sans succès. Peu à peu, l\'infection se répand mais plutôt que de tenter de se débarrasser des monstres, Lionel les enferme dans la cave de sa maison. Deux zombies, un prêtre et une infirmière, ont un rapport sexuel, ce qui donne naissance à un enfant zombie. En tentant de les mettre « à mort » définitivement, il se trompe de substance et leur donne des stimulants ce qui a pour effet de les rendre complètement enragés. Avec l\'aide de sa petite amie Paquita, Lionel réussit enfin à éliminer tous les zombies dont le plus sanguinaire, sa mère transformée en un gigantesque monstre, et ce de manière gorissime avec notamment une tondeuse à gazon.'
    },
    {
      MovieName: 'bad taste',
      AnneeDeSortie: 1987,
      Duree: '1h32',
      MovieKind: 'nanar',
      realisateur: 'peter jackson',
      // synopsis:'Une bande d\'extraterrestres anthropophages déguisés en humains envahissent un petit village néo-zélandais dans le but de « moissonner » des humains pour leur chaîne de fast-foods. Ils se voient repoussés par une poignée de résistants hargneux et armés jusqu\'aux dents.'
    },
    {
      MovieName: 'street trash',
      AnneeDeSortie: 1987,
      Duree: '1h42',
      MovieKind: 'nanar',
      realisateur: 'James Michael Muro',
      //synopsis:'Dans les tréfonds de sa réserve, le propriétaire d’une petite boutique de spiritueux découvre une caisse d’un alcool frelaté de marque Viper. Les bouteilles trouvent très vite preneurs, achetés par les marginaux et clochards qui peuplent une casse automobile du quartier. Ses effets sont dévastateurs, le breuvage d’origine inconnue décapant de l’intérieur ceux qui l’ingurgitent.'

    },
  ]

  @Input()
  routes: MovieModel[];
  destroy$: Subject<boolean> = new Subject();
  movies: MovieModel[] ;
  data: movies[];

  constructor(
    private nanarService: NanarService,
  ) {

  }

  ngOnInit(): void {
    this.routes = this.nanarService.AllRoutes;
    this.movies= this.listOfData
    this.nanarService.getAllNanarMovies$()
      .pipe(
        tap(x => console.log(x)),
        switchMap((movies =>  this.movies = [movies])),
        tap(x => console.log(x)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  onBack() {
    console.log('onBack')
  }
}
