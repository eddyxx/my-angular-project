import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SlasherRoutingModule } from './slasher-routing.module';
import { SlasherComponent } from './slasher.component';
import {NzDividerModule, NzPageHeaderModule, NzTableModule} from "ng-zorro-antd";


@NgModule({
  declarations: [SlasherComponent],
  imports: [
    CommonModule,
    SlasherRoutingModule,
    NzDividerModule,
    NzTableModule,
    NzPageHeaderModule
  ]
})
export class SlasherModule { }
