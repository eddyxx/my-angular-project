import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SlasherComponent } from './slasher.component';

const routes: Routes = [{ path: '', component: SlasherComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SlasherRoutingModule { }
