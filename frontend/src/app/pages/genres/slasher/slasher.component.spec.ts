import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlasherComponent } from './slasher.component';

describe('SlasherComponent', () => {
  let component: SlasherComponent;
  let fixture: ComponentFixture<SlasherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlasherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlasherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
