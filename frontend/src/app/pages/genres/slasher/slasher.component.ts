import {Component, Input, OnInit} from '@angular/core';
import {switchMap, takeUntil, tap} from "rxjs/operators";
import {SlasherService} from "../../../services/slasher.service";
import {MovieModel} from "../../../models/movie.model";
import {Subject} from "rxjs";

interface movies {

  MovieName: string;
  AnneeDeSortie: number;
  Duree: number| string;
  MovieKind: string;
  realisateur: string;

}

@Component({
  selector: 'app-slasher',
  templateUrl: './slasher.component.html',
  styleUrls: ['./slasher.component.less']
})
export class SlasherComponent implements OnInit {
  listOfData: movies[] = [
    {
      MovieName: 'maniac',
      AnneeDeSortie: 1980,
      Duree: '1h28',
      MovieKind: 'slasher',
      realisateur: 'William Lustig',
      //synopsis:'Victime des mauvais traitements infligés par sa mère lorsqu\'il était enfant, Frank Zito en est resté profondément traumatisé, hanté par d\'horribles rêves et impuissant. Un soir, dans l\'espoir d\'apaiser ses angoisses, il quitte son appartement sordide et se lance à la recherche d\'une prostituée dans les rues de New York. L\'expérience est un cuisant échec. Fou de rage, il étrangle la malheureuse et emporte son scalp comme trophée.'
    },
    {
      MovieName: 'halloween',
      AnneeDeSortie: 1978,
      Duree: '1h30',
      MovieKind: 'slasher',
      realisateur: 'John Carpenter',
      // synopsis:'La nuit d\'Halloween 1963. Le jeune Michael Myers se précipite dans la chambre de sa soeur aînée et la poignarde sauvagement. Après son geste, Michael se mure dans le silence et est interné dans un asile psychiatrique. Quinze ans plus tard, il s\'échappe de l\'hôpital et retourne sur les lieux de son crime. Il s\'en prend alors aux adolescents de la ville.'
    },
    {

      MovieName: 'massacre a la tronçonneuse',
      AnneeDeSortie: 1982,
      Duree: '1h30',
      MovieKind: 'slasher',
      realisateur: 'tobe hooper',
      // synopsis:'L\'histoire se concentre autour de la famille Hewitt qui habite dans un endroit reculé du Texas. L\'abattoir de la ville ferme et la région se vide ainsi de ses emplois. La famille décide de rester malgré tout et le jeune Thomas, licencié, se venge de son patron en le tuant. Le shérif veut l\'arrêter, mais son oncle abat le représentant de la loi et endosse désormais le costume de l\'autorité. Les Hewitt deviennent les maîtres de leur ville et pour subsister, ils n\'ont besoin que d\'un peu de nourriture que les badauds anonymes leur apporteront..'

    },
    {
      MovieName: 'vendredi 13',
      AnneeDeSortie: 1981,
      Duree: '1h36',
      MovieKind: 'slasher',
      realisateur: 'Sean S. Cunningham',
      // synopsis:'Fermé depuis une vingtaine d\'années suite à de mystérieux accidents, le camp de vacances "Crystal Lake" va être rouvert au public. Engagée comme cuisinière, l\'étudiante Annie fait de l\'auto-stop pour rejoindre ses compagnons. Montée dans un Jeep, elle est assassinée par son conducteur."'
    },
    {

      MovieName: 'scream',
      AnneeDeSortie: 1997,
      Duree: '1h51',
      MovieKind: 'slasher',
      realisateur: 'wes craven',
      // synopsis:'Casey Becker, une belle adolescente, est seule dans la maison familiale. Elle s\'apprête à regarder un film d\'horreur, mais le téléphone sonne. Au bout du fil, un serial killer la malmène, et la force à jouer à un jeu terrible : si elle répond mal à ses questions portant sur les films d\'horreur, celui-ci tuera son copain. Sidney Prescott sait qu\'elle est l\'une des victimes potentielles du tueur de Woodsboro. Celle-ci ne sait plus à qui faire confiance.'
    },
  ]
  @Input()
  routes: MovieModel[];
  destroy$: Subject<boolean> = new Subject();
  movies: MovieModel[] ;
  data: movies[];

  constructor(
    private slasherService: SlasherService,
  ) { }

  ngOnInit(): void {
    this.routes = this.slasherService.AllRoutes;
    this.movies= this.listOfData
    this.slasherService.getAllSlasherMovies$()
      .pipe(
        tap(x => console.log(x)),
        switchMap((movies =>  this.movies = [movies])),
        tap(x => console.log(x)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  onBack() {
    console.log('onBack')
  }
}
