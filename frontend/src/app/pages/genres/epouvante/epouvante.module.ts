import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EpouvanteRoutingModule } from './epouvante-routing.module';
import { EpouvanteComponent } from './epouvante.component';
import {NzDividerModule, NzPageHeaderModule, NzTableModule} from "ng-zorro-antd";


@NgModule({
  declarations: [EpouvanteComponent],
  imports: [
    CommonModule,
    EpouvanteRoutingModule,
    NzPageHeaderModule,
    NzDividerModule,
    NzTableModule
  ]
})
export class EpouvanteModule { }
