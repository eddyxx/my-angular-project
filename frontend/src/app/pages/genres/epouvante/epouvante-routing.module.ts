import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EpouvanteComponent } from './epouvante.component';

const routes: Routes = [{ path: '', component: EpouvanteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EpouvanteRoutingModule { }
