import {Component, Input, OnInit} from '@angular/core';
import {MovieModel} from "../../../models/movie.model";
import {Subject} from "rxjs";
import {switchMap, takeUntil, tap} from "rxjs/operators";
import {EpouvanteService} from "../../../services/epouvante.service";

interface movies {
  MovieName: string;
  AnneeDeSortie: number;
  Duree: number| string;
  MovieKind: string;
  realisateur: string;
}

@Component({
  selector: 'app-epouvante',
  templateUrl: './epouvante.component.html',
  styleUrls: ['./epouvante.component.less']
})
export class EpouvanteComponent implements OnInit {
  listOfData: movies[] = [
    {

      MovieName: 'Veronica ',
      AnneeDeSortie: 2017,
      Duree: '1h46',
      MovieKind: 'epouvante',
      realisateur: 'Paco Plaza',


    },
    {

      MovieName: 'Insidious',
      AnneeDeSortie: 2010,
      Duree: '1h43',
      MovieKind: 'epouvante',
      realisateur: 'James Wan',
    },
    {
      MovieName: 'Shutter',
      AnneeDeSortie: 2004,
      Duree: '1h37',
      MovieKind: 'epouvante',
      realisateur: 'Banjong Pisanthanakun, Parkpoom Wongpoom',

    },
    {
      MovieName: 'It follow',
      AnneeDeSortie: 2015,
      Duree: '1h47',
      MovieKind: 'epouvante',
      realisateur: 'David Robert Mitchell',

    },
    {

      MovieName: 'Mister Babadook',
      AnneeDeSortie: 2014,
      Duree: '1h35',
      MovieKind: 'epouvante',
      realisateur: 'Jennifer Kent',

    },
  ]

  destroy$: Subject<boolean> = new Subject();

  @Input() age: number ;
  routes: MovieModel[];
  movies: MovieModel[] ;
  data: movies[];


  constructor(
    private epouvanteService:EpouvanteService
  ) { }

  ngOnInit(): void {
    this.routes = this.epouvanteService.AllRoutes;
    this.movies= this.listOfData
    this.epouvanteService.getAllEpouvanteMovies$()
      .pipe(
        tap(x => console.log(x)),
        switchMap((movies =>  this.movies = [movies])),
        tap(x => console.log(x)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }


  onBack() {
    console.log('OnBack')
  }
}
