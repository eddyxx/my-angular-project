import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpouvanteComponent } from './epouvante.component';

describe('EpouvanteComponent', () => {
  let component: EpouvanteComponent;
  let fixture: ComponentFixture<EpouvanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpouvanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpouvanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
