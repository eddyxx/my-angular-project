import {Component, Input, OnInit} from '@angular/core';
import {MovieModel} from "../../../models/movie.model";
import {Subject} from "rxjs";
import {ComediehorrifiqueService} from "../../../services/comediehorrifique.service";
import {switchMap, takeUntil, tap} from "rxjs/operators";

interface movies {

  MovieName: string;
  AnneeDeSortie: number;
  Duree: number| string;
  MovieKind: string;
  realisateur: string;
}

@Component({
  selector: 'app-comediehorrifique',
  templateUrl: './comediehorrifique.component.html',
  styleUrls: ['./comediehorrifique.component.less']
})
export class ComediehorrifiqueComponent implements OnInit {
  listOfData: movies[] = [
    {

      MovieName: 'jusquen enfer',
      AnneeDeSortie: 2009,
      Duree: '1h39',
      MovieKind: 'comédie/horreur',
      realisateur: 'sam raimi',
      // synopsis:'Christine Brown travaille dans une société de crédit. Elle espère vraiment avoir le poste d\'assistant qui vient de se libérer. Alors, quand madame Ganush, une vieille gitane, vient lui demander un prêt pour pouvoir conserver son logement, elle le lui refuse pour montrer à son supérieur, monsieur Jacks, qu\'elle est capable de ce genre de décision."\n'

    },
    {

      MovieName: 'shaun of the dead',
      AnneeDeSortie: 2004,
      Duree: '1h40',
      MovieKind: 'comédie/horreur',
      realisateur: 'Edgar wright',
      // synopsis:'Shaun ne sait pas quoi faire de sa vie, et encore moins de sa relation de couple. Heureusement, une hallucinante invasion de zombies va lui remettre les idées en place.'
    },
    {
      MovieName: 'evil dead 2',
      AnneeDeSortie: 1987,
      Duree: '1h24',
      MovieKind: 'comédie horrifique',
      realisateur: 'sami raimi',
      // synopsis:'Ash et Linda retournent dans la cabane où, quatre ans plus tôt, ils avaient été attaqués par les démons qui hantaient les bois environnants'
    },
    {
      MovieName: 'tucker et dale fightent le mal',
      AnneeDeSortie: 2008,
      Duree: '1h29',
      MovieKind: 'comédie horrifique',
      realisateur: 'Eli craig',
      //synopsis:'Tucker et Dale sont deux gentils péquenauds venus se ressourcer en forêt. Ils y rencontrent des étudiants venus faire la fête. Suite à un quiproquo entraînant la mort d\'un des jeunes, ces derniers pensent que Tucker et Dale sont des serial killers qui veulent leur peau, alors que nos héros pensent que les jeunes font partie d\'une secte et qu\'ils sont là pour un suicide collectif ! C\'est le début d\'un gigantesque malentendu dans lequel horreur et hilarité vont se mélanger.'
    },
    {

      MovieName: 'bienvenue a zombieland',
      AnneeDeSortie: 2009,
      Duree: '1h28',
      MovieKind: 'comédie horrifique',
      realisateur: 'ruben fleischer',
      //synopsis:'Suite à une mutation du virus de la vache folle, les humains sont transformés en zombies. Deux survivants que tout oppose, sillonnent les routes et affrontent les zombies qui grouillent aux quatre coins du pays'
    },
  ]



  @Input() age: number ;
  routes: MovieModel[];
  destroy$: Subject<boolean> = new Subject();
  movies: MovieModel[] ;
  data: movies[];


  constructor(
    private comediehorrifiqueService:ComediehorrifiqueService
  ) { }

  ngOnInit(): void {
    this.routes = this.comediehorrifiqueService.AllRoutes;
    this.movies= this.listOfData
    this.comediehorrifiqueService.getAllComHMovies$()
      .pipe(
        tap(x => console.log(x)),
        switchMap((movies =>  this.movies = [movies])),
        tap(x => console.log(x)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }


  onBack() {
    console.log('OnBack')
  }
}

