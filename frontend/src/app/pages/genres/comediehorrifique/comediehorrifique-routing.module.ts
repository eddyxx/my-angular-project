import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComediehorrifiqueComponent } from './comediehorrifique.component';

const routes: Routes = [
  {
    path: '', component: ComediehorrifiqueComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComediehorrifiqueRoutingModule { }
