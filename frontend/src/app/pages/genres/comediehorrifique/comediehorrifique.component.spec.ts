import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComediehorrifiqueComponent } from './comediehorrifique.component';

describe('ComediehorrifiqueComponent', () => {
  let component: ComediehorrifiqueComponent;
  let fixture: ComponentFixture<ComediehorrifiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComediehorrifiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComediehorrifiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
