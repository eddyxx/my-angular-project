import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComediehorrifiqueRoutingModule } from './comediehorrifique-routing.module';
import { ComediehorrifiqueComponent } from './comediehorrifique.component';
import {
    NzDescriptionsModule,
    NzDividerModule,
    NzDrawerModule,
    NzListModule,
    NzPageHeaderModule,
    NzTableModule
} from "ng-zorro-antd";
import {SfhorreurModule} from "../sfhorreur/sfhorreur.module";


@NgModule({
  declarations: [ComediehorrifiqueComponent],
    imports: [
        CommonModule,
        ComediehorrifiqueRoutingModule,
        NzPageHeaderModule,
        SfhorreurModule,
        NzListModule,
        NzDrawerModule,
        NzDescriptionsModule,
        NzDividerModule,
        NzTableModule
    ]
})
export class ComediehorrifiqueModule { }
