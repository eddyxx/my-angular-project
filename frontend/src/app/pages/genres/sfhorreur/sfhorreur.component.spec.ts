import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SfhorreurComponent } from './sfhorreur.component';

describe('SfhorreurComponent', () => {
  let component: SfhorreurComponent;
  let fixture: ComponentFixture<SfhorreurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SfhorreurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SfhorreurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
