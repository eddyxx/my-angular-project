import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SfhorreurRoutingModule } from './sfhorreur-routing.module';
import { SfhorreurComponent } from './sfhorreur.component';
import {NzDividerModule, NzPageHeaderModule, NzTableModule} from "ng-zorro-antd";


@NgModule({
  declarations: [SfhorreurComponent],
  exports: [
    SfhorreurComponent
  ],
  imports: [
    CommonModule,
    SfhorreurRoutingModule,
    NzPageHeaderModule,
    NzDividerModule,
    NzTableModule
  ]
})
export class SfhorreurModule { }
