import {Component, Input, OnInit} from '@angular/core';
import {MovieModel} from "../../../models/movie.model";
import {switchMap, takeUntil, tap} from "rxjs/operators";
import {SfhorreurService} from "../../../services/sfhorreur.service";
import {Subject} from "rxjs";

interface movies {

  MovieName: string;
  AnneeDeSortie: number;
  Duree: number| string;
  MovieKind: string;
  realisateur: string;

}

@Component({
  selector: 'app-sfhorreur',
  templateUrl: './sfhorreur.component.html',
  styleUrls: ['./sfhorreur.component.less']
})
export class SfhorreurComponent implements OnInit {
  listOfData: movies[] = [
    {
      MovieName: 'alien',
      AnneeDeSortie: 1979,
      Duree: '1h57',
      MovieKind: 'sf/horreur',
      realisateur: 'ridley scott',
      //synopsis:'Durant le voyage de retour d\'un immense cargo spatial en mission commerciale de routine, ses passagers, cinq hommes et deux femmes plongés en hibernation, sont tirés de leur léthargie dix mois plus tôt que prévu par Mother, l\'ordinateur de bord. Ce dernier a en effet capté dans le silence interplanétaire des signaux sonores, et suivant une certaine clause du contrat de navigation, les astronautes sont chargés de prospecter tout indice de vie dans l\'espace .'
    },
    {
      MovieName: 'predator',
      AnneeDeSortie: 1987,
      Duree: '1h47',
      MovieKind: 'sf/horreur',
      realisateur: 'John McTiernan',
      //synopsis:'Le major Dutch Schaeffer prend la tête d\'un commando chargé de délivrer un groupe de civils américains prisonniers de guérilleros en Amérique centrale. Largués en hélicoptère dans une jungle hostile, ses hommes et lui découvrent des corps de soldats écorchés vifs... avant d\'être pris en chasse par une mystérieuse créature.'
    },
    {
      MovieName: 'ghost to mars',
      AnneeDeSortie: 2001,
      Duree: '1h38',
      MovieKind: 'sf/horreur',
      realisateur: 'john carpenter',
      // synopsis:'En 2176, Mars, la planète rouge, est colonisée par les êtres humains pour éviter la surpopulation sur Terre. Son sol riche en ressources naturelles permet à plusieurs milliers de personnes de survivre et de travailler. Une équipe de mineurs tombent d\'ailleurs sur une sépulture et libèrent par le même coup les âmes d\'anciens guerriers d\'une race martienne. Ces derniers décident de se venger en s\'introduisant dans les corps humains pour les éliminer'
    },
    {
      MovieName: 'the void',
      AnneeDeSortie: 2016,
      Duree: '1h30',
      MovieKind: 'sf/horreur',
      realisateur: 'Steven Kostanski, Jeremy Gillespie',
      // synopsis:'Un agent de police découvre un homme ensanglanté sur une route déserte et l\'emmène en urgence à l\'hôpital. Rapidement, d\'étranges individus entourent le lieu et les patients commencent à devenir fous. Le policier va alors découvrir, à l\'intérieur de l\'hôpital, une porte vers un monde démoniaque.'
    },
    {

      MovieName: 'prometheus',
      AnneeDeSortie: 2012,
      Duree: '2h',
      MovieKind: 'sf/horreur',
      realisateur: 'ridley scott',
      // synopsis:'En 2093, des scientifiques cherchent à dépasser leurs limites mentales et physiques et tentent d\'explorer ce qu\'il y a au-delà du possible. Ils vont être amenés à découvrir un indice sur l\'origine de l\'humanité sur Terre. Cette découverte les entraîne dans un voyage fascinant jusqu\'aux recoins les plus sombres de l\'univers. Là-bas, un affrontement terrifiant qui décidera de l\'avenir de l\'humanité les attend.'
    },
  ]
  @Input()
  routes: MovieModel[];
  destroy$: Subject<boolean> = new Subject();
  movies: MovieModel[] ;
  data: movies[];



  constructor(
    private sfhorreurService: SfhorreurService,
  ) {

  }

  ngOnInit(): void {
    this.routes = this.sfhorreurService.AllRoutes;
    this.movies= this.listOfData
    this.sfhorreurService.getAllSfHorreurMovies$()
      .pipe(
        tap(x => console.log(x)),
        switchMap((movies =>  this.movies = [movies])),
        tap(x => console.log(x)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  onBack() {
    console.log('onBack')
  }
}
