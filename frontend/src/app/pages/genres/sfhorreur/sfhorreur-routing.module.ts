import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SfhorreurComponent } from './sfhorreur.component';

const routes: Routes = [{ path: '', component: SfhorreurComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SfhorreurRoutingModule { }
