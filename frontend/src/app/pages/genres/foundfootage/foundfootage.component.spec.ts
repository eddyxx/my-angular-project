import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundfootageComponent } from './foundfootage.component';

describe('FoundfootageComponent', () => {
  let component: FoundfootageComponent;
  let fixture: ComponentFixture<FoundfootageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoundfootageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundfootageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
