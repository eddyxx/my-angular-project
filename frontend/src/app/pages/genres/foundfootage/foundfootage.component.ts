import {Component, Input, OnInit} from '@angular/core';
import {Subject} from "rxjs";
import {MovieModel} from "../../../models/movie.model";
import {switchMap, takeUntil, tap} from "rxjs/operators";
import {FoundfootageService} from "../../../services/foundfootage.service";
interface movies {

  MovieName: string;
  AnneeDeSortie: number;
  Duree: number| string;
  MovieKind: string;
  realisateur: string;

}
@Component({
  selector: 'app-foundfootage',
  templateUrl: './foundfootage.component.html',
  styleUrls: ['./foundfootage.component.less']
})
export class FoundfootageComponent implements OnInit {
  listOfData: movies[] = [
{
  MovieName: 'creep',
  AnneeDeSortie: 2015,
  Duree: '1h22',
  MovieKind: 'found footage',
  realisateur: 'Patrick Kack-Brice',
  //synopsis:'Aaron, un réalisateur sans le sou, accepte de se rendre dans un chalet éloigné pour un client, Josef. Josef explique à Aaron qu\'il ne lui reste que peu de temps à vivre car il est atteint d\'une tumeur au cerveau inopérable. Il va mourir avant que sa femme enceinte, Angela n\'accouche et demande à Aaron de l\'aider à enregistrer un journal vidéo pour son enfant à naître. Mais Josef est excentrique, ce qui met Aaron mal à l\'aise jusqu\'au point où Josef révèle qu\'il a violé sa femme. Se sentant en danger, Aaron cherche ses clefs de voiture mais ne les trouve pas. Il intercepte un appel téléphonique d\'Angela, qui est en fait la sœur de Josef qui l\'enjoint à fuir le plus vite possible.'
},
    {

      MovieName: 'rec',
      AnneeDeSortie: 2005,
      Duree: '1h18',
      MovieKind: 'found footage',
      realisateur: 'Jaume Balagueró, Paco Plaza',
      //synopsis:'Une jeune journaliste et un collègue suivent des pompiers pour une intervention dans un appartement. Prise à son propre piège, la journaliste est enfermée avec le scoop qu\'elle désirait ardemment. Elle embarque le spectateur dans un huis clos terrifiant quand un nouveau virus transforme les infectés en dangereux zombies.'


    },
    {
      MovieName: 'le projet blair witch',
      AnneeDeSortie: 1999,
      Duree: '1h57',
      MovieKind: 'found footage',
      realisateur: 'Eduardo Sánchez, Daniel Myrick',
      // synopsis:'Le 21 octobre 1994, 3 étudiants de cinéma vont dans la forêt de Black Hills, dans le Maryland, pour faire un documentaire sur une légende locale : la sorcière de Blair. Un an plus tard, leur bande vidéo est retrouvée. Eux sont toujours portés disparus."'
    },
    {
      MovieName: 'le projet atticus',
      AnneeDeSortie: 2015,
      Duree: '1h32',
      MovieKind: 'found footage',
      realisateur: 'Chris Sparling',
      // synopsis:'Fondé en 1976 par le Dr Henry West, l\'Institut Atticus était spécialisé dans l\'étude de personnes développant des capacités paranormales : parapsychologie, voyance, psychokinésie, etc. Des centaines de personnes présentant ce genre d\'aptitudes ont été étudiées par les chercheurs de l\'institut et de nombreux articles annonçant leurs résultats ont été publiés. Aucun cas étudié jusque-là n\'avait préparé le Dr West et son équipe à l\'arrivée de Judith Winstead."'
    },
    {
      MovieName: 'paranormal acctivity',
      AnneeDeSortie: 2009,
      Duree: '1h10',
      MovieKind: 'found footage',
      realisateur: 'oren peli',
      // synopsis:'Un jeune couple s\'installe dans une maison en banlieue. Le bonheur fait place à la terreur lorsqu\'ils doivent repousser, toutes les nuits, les attaques d\'un esprit démoniaque...'
    },
  ]
  @Input() age: number ;
  routes: MovieModel[];
  destroy$: Subject<boolean> = new Subject();
  movies: MovieModel[] ;
  data: movies[];

  constructor(
    private foundfootageService: FoundfootageService,
  ) {

  }

  ngOnInit(): void {
    this.routes = this.foundfootageService.AllRoutes;
    this.movies= this.listOfData
    this.foundfootageService.getAllFoundFootageMovies$()
      .pipe(
        tap(x => console.log(x)),
        switchMap((movies =>  this.movies = [movies])),
        tap(x => console.log(x)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }


  onBack() {
    console.log('OnBack')
  }
}




