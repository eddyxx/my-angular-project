import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoundfootageRoutingModule } from './foundfootage-routing.module';
import { FoundfootageComponent } from './foundfootage.component';
import {NzDividerModule, NzPageHeaderModule, NzTableModule} from "ng-zorro-antd";


@NgModule({
  declarations: [FoundfootageComponent],
  imports: [
    CommonModule,
    FoundfootageRoutingModule,
    NzPageHeaderModule,
    NzTableModule,
    NzDividerModule
  ]
})
export class FoundfootageModule { }
