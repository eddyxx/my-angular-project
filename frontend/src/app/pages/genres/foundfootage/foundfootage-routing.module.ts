import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FoundfootageComponent } from './foundfootage.component';

const routes: Routes = [{ path: '', component: FoundfootageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoundfootageRoutingModule { }
