import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies.component';
import {
  NzCardModule, NzDescriptionsModule, NzDividerModule,
  NzDrawerModule,
  NzGridModule,
  NzIconModule,
  NzListModule,
  NzPageHeaderModule, NzSkeletonModule, NzSwitchModule, NzTableModule
} from "ng-zorro-antd";
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [MoviesComponent],
  exports: [
    MoviesComponent
  ],
    imports: [
        CommonModule,
        MoviesRoutingModule,
        NzPageHeaderModule,
        NzListModule,
        NzIconModule,
        NzGridModule,
        NzCardModule,
        NzDrawerModule,
        NzDescriptionsModule,
        NzDividerModule,
        NzSwitchModule,
        FormsModule,
        NzSkeletonModule,
        NzTableModule,

    ]
})
export class MoviesModule {

 }
