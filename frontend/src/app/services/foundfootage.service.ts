import { Injectable } from '@angular/core';
import {MovieModel} from "../models/movie.model";
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FoundfootageService {

  AllRoutes: MovieModel[];
  root:string = environment.databaseURL;

constructor(
  private httpClient: HttpClient
) {

}
getAllFoundFootageMovies$(): Observable<MovieModel>{
  return this.httpClient.get(this.root + 'movies') as Observable<MovieModel>
}
getFoundFootageMovieById$(movieId:string): Observable<MovieModel> {
  return this.httpClient.get(this.root + movieId)as Observable<MovieModel>;
}
}
