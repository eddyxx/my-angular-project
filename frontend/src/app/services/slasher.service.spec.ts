import { TestBed } from '@angular/core/testing';

import { SlasherService } from './slasher.service';

describe('SlasherService', () => {
  let service: SlasherService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SlasherService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
