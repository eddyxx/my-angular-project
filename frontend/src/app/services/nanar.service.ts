import { Injectable } from '@angular/core';
import {MovieModel} from "../models/movie.model";
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class NanarService {
  AllRoutes: MovieModel[];

  root:string = environment.databaseURL;
  constructor(
    private httpClient: HttpClient
  ) {

  }
  getAllNanarMovies$(): Observable<MovieModel>{
    return this.httpClient.get(this.root + 'movies') as Observable<MovieModel>
  }
  getNanarById$(movieId:string): Observable<MovieModel> {
    return this.httpClient.get(this.root + movieId)as Observable<MovieModel>;
  }
}
