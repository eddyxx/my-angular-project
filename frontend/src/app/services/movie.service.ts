import { Injectable } from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {MovieModel} from "../models/movie.model";

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  AllRoutes: MovieModel[] = [];



  root:string = environment.databaseURL;
  constructor(
    private httpClient: HttpClient
  ) {

  }
  getAllMovies$(): Observable<MovieModel>{
    return this.httpClient.get(this.root + 'movies') as Observable<MovieModel>
}
  getMovieById$(movieId:string): Observable<MovieModel> {
    return this.httpClient.get(this.root + movieId)as Observable<MovieModel>;
  }
getMovieByKind$(movieKind:string): Observable<MovieModel>{
    return this.httpClient.get(this.root + movieKind) as Observable<MovieModel>;
}
}

