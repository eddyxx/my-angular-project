import { TestBed } from '@angular/core/testing';

import { EpouvanteService } from './epouvante.service';

describe('EpouvanteService', () => {
  let service: EpouvanteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EpouvanteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
