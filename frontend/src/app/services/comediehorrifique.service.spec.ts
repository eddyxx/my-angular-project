import { TestBed } from '@angular/core/testing';

import { ComediehorrifiqueService } from './comediehorrifique.service';

describe('ComediehorrifiqueService', () => {
  let service: ComediehorrifiqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComediehorrifiqueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
