import { TestBed } from '@angular/core/testing';

import { SfhorreurService } from './sfhorreur.service';

describe('SfhorreurService', () => {
  let service: SfhorreurService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SfhorreurService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
