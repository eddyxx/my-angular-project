import { Injectable } from '@angular/core';
import {MovieModel} from "../models/movie.model";
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class EpouvanteService {

  AllRoutes: MovieModel[];
  root:string = environment.databaseURL;

  constructor(
    private httpClient:HttpClient
  ) {

  }
  getAllEpouvanteMovies$(): Observable<MovieModel>{
    return this.httpClient.get(this.root + 'movies') as Observable<MovieModel>
  }
  getEpouvanteById$(movieId:string): Observable<MovieModel> {
    return this.httpClient.get(this.root + movieId)as Observable<MovieModel>;
  }
}
