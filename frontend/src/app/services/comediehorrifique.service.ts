import { Injectable } from '@angular/core';
import {MovieModel} from "../models/movie.model";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ComediehorrifiqueService {

  AllRoutes: MovieModel[];
  root:string = environment.databaseURL;

  constructor(
    private httpClient:HttpClient
  ) {

  }
  getAllComHMovies$(): Observable<MovieModel>{
    return this.httpClient.get(this.root + 'movies') as Observable<MovieModel>
  }
  getComHById$(movieId:string): Observable<MovieModel> {
    return this.httpClient.get(this.root + movieId)as Observable<MovieModel>;
  }
}
