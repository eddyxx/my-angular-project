import { Injectable } from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ListModel} from "../models/list.model";
import {tap} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class ListService {
root: string = environment.apiKey;
  constructor(
    private httpClient: HttpClient
  ) {

  }
  getListById$(listId:string): Observable<ListModel>{
    return this.httpClient.get(this.root + listId)as Observable<ListModel>;
  }
  getAllList$(): Observable<ListModel[]>{
    return this.httpClient.get(this.root + 'lists') as Observable<ListModel[]>;
  }
  createNewList$(list:ListModel): Observable<ListModel> {
    return (this.httpClient
      .post(this.root + 'lists', list) as Observable<ListModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }

  deleteList$(listId: string): Observable<string> {
    return (this.httpClient
        .delete(this.root+'lists/'+ listId)as Observable<string>)
      .pipe(
        tap(x=> console.log(x))
    )
  }
   updateUser$(list: string, listId: string): Observable<ArrayBuffer> {
   return (this.httpClient
   .patch(this.root + 'lists/' + list , listId ) as Observable<ArrayBuffer>)
   .pipe(
    tap(x => console.log(x)),
    );
  }

}
