import { TestBed } from '@angular/core/testing';

import { NanarService } from './nanar.service';

describe('NanarService', () => {
  let service: NanarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NanarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
