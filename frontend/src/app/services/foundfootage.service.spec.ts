import { TestBed } from '@angular/core/testing';

import { FoundfootageService } from './foundfootage.service';

describe('FoundfootageService', () => {
  let service: FoundfootageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FoundfootageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
