import {Component, OnInit} from '@angular/core';
import {MovieService} from "./services/movie.service";
import {ListService} from "./services/list.service";
import {tap} from "rxjs/operators";
import {UserService} from "./services/user.service";
import {VideosService} from "./services/videos.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {

  isCollapsed = false;
  title = 'cineNigntmare';
  listId: string;
  list: string;
  movieId: string;
  MovieKind:string;
  private message: string;

constructor(
  private movieService: MovieService,
  private listService: ListService,
  private userService: UserService,
  private videoService: VideosService


){

}
  ngOnInit(): void{

  }
    createList(): void {
      this.listService.createNewList$({
        Movies: [],
        nameList: 'a voir',
      })
        .subscribe();
  }
  deleteList() {
    return this.listService.deleteList$('YIcxj3cKDnsFdNUIO0ly')
      .subscribe();

  }
  getAllList() {
  return this.listService.getAllList$()
    .pipe(
      tap(x => console.log(x))
    )
    .subscribe();
  }
  getListById(){
  return this.listService.getListById$(this.listId)
    .pipe(
      tap(x => console.log(x))
     )
     .subscribe();
  }
  updateList(){
  return this.listService.updateUser$(this.list,this.listId)
    .pipe(
      tap(x=> console.log(x))

    )
  .subscribe();

  }

  getAllMovie(){
  return this.movieService.getAllMovies$()
    .pipe(
      tap(x=> console.log(x))
    )
    .subscribe();
  }
  getMovieById(){
  return this.movieService.getMovieById$(this.movieId)
    .pipe(
      tap(x=> console.log(x))
    )
  }
  getMovieByKind(){
  return this.movieService.getMovieByKind$(this.MovieKind)
  }
}
